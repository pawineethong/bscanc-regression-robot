import xml.dom.minidom as minidom
import pymqi
import os

os.environ['MQCHLLIB']='/root/pythonmq/'
os.environ['MQCHLTAB']='APPTDECH.TAB'
class connect_mq:
    def connectMQmanager(self,queue_manager, channel,host,port):
        user = 'mqm'
        password = 'P@ssw0rd'
        conn_info = '%s(%s)' % (host, port)
        qmgr = pymqi.connect(queue_manager, channel, conn_info,user,password)
        return qmgr

    def connectQueue(self,qmgr_con,queue_name):
        queue = pymqi.Queue(qmgr_con, queue_name)
        return queue

    def put_message(self,queue,msg_xml):
        queue.put(msg_xml)

    def closeConnection(self,qmgr,queue):
        queue.close()
        qmgr.disconnect()

    def disConnection(self,qmgr):
        qmgr.disconnect()

    def put_mq(self,queue,msg_xml):
        # doc = minidom.parse(msg_xml)
        # queue.put(doc.toprettyxml())
        queue.put(msg_xml)
        queue.close()
        # queue.disconnect()
*** Keywords ***
connect to ipa database
    ${db_connect}    connect    ${db.ipa.username}    ${db.ipa.password}    ${db.ipa.conn_str}
    set test variable    ${db_connect}

close ipa database connection
    disconnect

query number of transaction by transaction id
    ${sql}=   set variable    SELECT COUNT(*) FROM MT_TRANSACTION_MESSAGE WHERE transaction_ident = '${tran_id}'
    ${result}=    query_all    ${db_connect}    ${sql}
    ${count}=    set variable    ${result[0]}
    [Return]     ${count["COUNT(*)"]}

query number of transaction by instr id
    ${sql}=   set variable    SELECT COUNT(*) FROM MT_TRANSACTION_MESSAGE WHERE transaction_ident = '${instr_id}'
    ${result}=    query_all    ${db_connect}    ${sql}
    ${count}=    set variable    ${result[0]}
    [Return]     ${count["COUNT(*)"]}

query transaction of mt_transaction
    ${sql}=   set variable    SELECT * FROM MT_TRANSACTION WHERE transaction_ident = '${instr_id}' order by INSERT_TIMESTAMP DESC
    ${result}=    query_all    ${db_connect}    ${sql}
    ${result_text}=    set variable    ${result[0]}
    [Return]     ${result_text}
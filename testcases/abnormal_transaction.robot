*** Settings ***
Resource  ../../resources/imports.robot
Library     DatabaseLibrary

*** Test Cases ***
TC001_ISO20022_BSC_ABNORMAL Credit Transfer : Recipient decline response message (PACS.002)
    [Setup]    run keywords    mq manager connection site1
    ...                        connect to ipa database
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    And connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct_reject' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction rtp and ct to database
    [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq

TC002_ISO20022_BSC_ABNORMAL Credit Transfer : ITMX not receive message PACS.008 (not put transaction to queue)
    [Setup]    run keywords    mq manager connection site1
    ...                        connect to ipa database
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    And connect mq request ct to ips of bank 'cmbt'
    Then should insert transaction to ipa '2' record to database
    And disconnect mq

#TC003_ISO20022_BSC_ABNORMAL >>  Test Menaul
#TC004_1_ISO20022_BSC_ABNORMAL Credit Transfer : Receiving bank connection is not avaliable

TC004_2_ISO20022_BSC_ABNORMAL Credit Transfer : Receiving bank sign off
    [Setup]    run keywords  mq manager connection site1
    ...                      send singon to 'cmbt' site1
    ...                      connect to ipa database
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    # SIGN OFF KTB
    And connect mq request to ips of bank 'ktb'
    And sleep  2s
    And send request sign off message 'signoff' to 'ktb'
    And sleep  6s
    # CT LEG1
    And connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    Then should insert transaction to ipa '4' record to database
    And should insert status 'REJECTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    send singon to 'ktb' site1
    ...    disconnect mq

TC004_3_ISO20022_BSC_ABNORMAL Credit Transfer : Receiving bank Suspend
    [Tags]  menaul
    [Setup]    run keywords  mq manager connection site1
    ...                      send singon to 'cmbt' site1
    ...                      connect to ipa database
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    # Suspend KTB
    And sleep  10s
    # CT LEG1
    And connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    Then should insert transaction to ipa '4' record to database
    And should insert status 'REJECTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq

TC005_ISO20022_BSC_ABNORMAL Credit Transfer : Response message (PACS.002) cannot send back to ITMX
    [Setup]    run keywords  mq manager connection site1
    ...                      send singon to 'cmbt' site1
    ...                      connect to ipa database
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    And connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    And sleep  20s
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction to ipa '8' record to database
    And should insert status 'REJECTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    send singon to 'ktb' site1
    ...    disconnect mq

TC006_ISO20022_BSC_ABNORMAL Credit Transfer : Receiving bank not response message (PACS.002)
    [Setup]    run keywords  mq manager connection site1
    ...                      send singon to 'cmbt' site1
    ...                      connect to ipa database
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    And connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    And sleep  15s
    Then should insert transaction to ipa '7' record to database
    And should insert status 'REJECTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq

TC007_ISO20022_BSC_ABNORMAL Credit Transfer : ITMX cannot send message (PACS.002) to sending bank
    [Setup]    run keywords  mq manager connection site1
    ...                      send singon to 'cmbt' site1
    ...                      connect to ipa database
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    And connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    # Send CT Leg3
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.00'
    # Send CT Leg1 Round 2 << DUPL >>
    And sleep  2s
    And connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    # Check transaction on database ipa
    Then should insert transaction to ipa '8' record to database
    And should insert status 'REJECTED' to ipa
    And should insert DUPL flag 'Y' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq

TC008_ISO20022_BSC_ABNORMAL Message format fail / Structure incorrect / Signature fail
    [Setup]    run keywords    connect to ipa database
    ...                        mq manager connection site1
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp_format_fail' from 'ktb' to 'cmbt' with amount '29.00'
    # Check transaction menaul
   [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq

TC009_ISO20022_BSC_ABNORMAL Void transaction : Overcut-off time (after 23:00:00)
    [Setup]    run keywords    mq manager connection site1
    ...                        connect to ipa database
    #  RTP AND CT - SUCCESS TRANSACTIONf
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    Then connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction to ipa '6' record to database
    And should insert status 'ACCEPTED' to ipa
    #   VOID TRANSACTION
    Given connect mq request void to ips of bank 'cmbt'
    When send request void 'leg1_void' from 'cmbt' to 'ktb' with amount '29.00'
    Then connect mq respons to ips from bank 'ktb'
    And send response reject void transaction 'void_over_cuttime' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction to ipa '4' record to database
    And should insert status 'REJECTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq

TC012_1_ISO20022_BSC_ABNORMAL Void transaction : Receiving bank sign off
    [Setup]    run keywords  mq manager connection site1
    ...                      send singon to 'ktb' site1
    ...                      connect to ipa database
    #  RTP AND CT - SUCCESS TRANSACTIONf
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    Then connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction to ipa '6' record to database
    And should insert status 'ACCEPTED' to ipa
    # SIGN OFF KTB
    And connect mq request to ips of bank 'ktb'
    And sleep  2s
    And send request sign off message 'signoff' to 'ktb'
#    And sleep  2s
#    #   VOID TRANSACTION
    Given connect mq request void to ips of bank 'cmbt'
    When send request void 'leg1_void' from 'cmbt' to 'ktb' with amount '29.00'
    Then should insert transaction to ipa '2' record to database
    And should insert status 'REJECTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    send singon to 'ktb' site1
    ...    disconnect mq

TC012_2_ISO20022_BSC_ABNORMAL Void transaction : Receiving bank Suspend
    [Tags]  menaul
    [Setup]    run keywords  mq manager connection site1
    ...                      send singon to 'cmbt' site1
    ...                      connect to ipa database
    #  RTP AND CT - SUCCESS TRANSACTIONf
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    Then connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.00'
    log to console   "rtp and ct finish"
    Then should insert transaction to ipa '6' record to database
    And should insert status 'ACCEPTED' to ipa
    # Suspend KTB
    And sleep  10s
    #   VOID TRANSACTION
    Given connect mq request void to ips of bank 'cmbt'
    When send request void 'leg1_void' from 'cmbt' to 'ktb' with amount '29.00'
    Then should insert transaction to ipa '2' record to database
    And should insert status 'REJECTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq

TC013_ISO20022_BSC_ABNORMAL Void transaction : Customer bank not response
    [Setup]    run keywords  mq manager connection site1
    ...                      send singon to 'ktb' site1
    ...                      connect to ipa database
        #  RTP AND CT - SUCCESS TRANSACTIONf
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    Then connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction to ipa '6' record to database
    And should insert status 'ACCEPTED' to ipa
    #   VOID TRANSACTION
    Given connect mq request void to ips of bank 'cmbt'
    When send request void 'leg1_void' from 'cmbt' to 'ktb' with amount '29.00'
    sleep  2s
    Then should insert transaction to ipa '5' record to database
    And should insert status 'REJECTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq

TC014_ISO20022_BSC_ABNORMAL Void transaction : Sending bank not response
    [Tags]  menaul
    [Setup]    run keywords  mq manager connection site1
    ...                      send singon to 'cmbt' site1
    ...                      connect to ipa database
        #  RTP AND CT - SUCCESS TRANSACTIONf
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    Then connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction to ipa '6' record to database
    And should insert status 'ACCEPTED' to ipa
    log to console    rtp and ct are finish
    #   VOID TRANSACTION LEG1
    Given connect mq request void to ips of bank 'cmbt'
    When send request void 'leg1_void' from 'cmbt' to 'ktb' with amount '29.00'
    # Suspend CMBT
    And sleep  10s
    #   VOID TRANSACTION LEG3 >> Success Transaction
    Then connect mq respons to ips from bank 'ktb'
    And send response void 'leg3_void' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction to ipa '4' record to database
    And should insert status 'ACCEPTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq

TC015_ISO20022_BSC_ABNORMAL Void transaction : Reciving bank not response
    [Tags]  menaul
    [Setup]    run keywords  mq manager connection site1
    ...                      send singon to 'cmbt' site1
    ...                      connect to ipa database
        #  RTP AND CT - SUCCESS TRANSACTIONf
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    Then connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction to ipa '6' record to database
    And should insert status 'ACCEPTED' to ipa
    log to console    rtp and ct are finish
    #   VOID TRANSACTION LEG1
    Given connect mq request void to ips of bank 'cmbt'
    When send request void 'leg1_void' from 'cmbt' to 'ktb' with amount '29.00'
    # Suspend KTB
    And sleep  15s
    #   VOID TRANSACTION LEG3
    Then connect mq respons to ips from bank 'ktb'
    And send response void 'leg3_void' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction to ipa '4' record to database
    And should insert status 'ACCEPTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq

TC000_ISO20022_BSC LOOKUP AND ACCOUNT VERIFICATION FOR CREDIT TRANSFER
    [Setup]    run keywords    mq manager connection site1
    ...                        connect to ipa database
    ...                        send singon to 'ktb' site1
    Given connect mq request to ips of bank 'ktb'
    When send request message av 'leg1_av' of 'ktb2cmbt' to MQ
    And connect mq respons to ips from bank 'cmbt'
    And send response message av 'leg3_av' to MQ
    #### Credit Transfer ####
#    Then sleep  2s
#    Given connect mq request to ips of bank 'ktb'
#    When send request ct/av message 'leg1_ct' of 'ktb2cmbt' to MQ
#    And connect mq respons to ips from bank 'cmbt'
#    And send response ct/av message 'leg3_ct' to MQ
#    Then should insert transaction to database
#    Then should insert transaction ct to database
    [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq
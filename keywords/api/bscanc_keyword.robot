*** Keywords ***
send request to rtp and ct success from '${sending}' to '${receiving}'
    connect to ipa database
    connect mq request to ips of bank '${sending}'
    send request rtp 'leg1_rtp' from '${sending}' to '${receiving}' with amount '29.00'
    connect mq request ct to ips of bank '${receiving}'
    send request ct 'leg1_ct' from '${receiving}' to '${sending}' with amount '29.00'
    connect mq respons to ips from bank '${sending}'
    send response ct 'leg3_ct' from '${sending}' to '${receiving}' with amount '29.00'
    should insert transaction rtp and ct to database
    close ipa database connection
    disconnect mq

send request to rtp '${sending}' to '${receiving}' not success
    connect to ipa database
    connect mq request to ips of bank '${sending}'
    send request rtp 'leg1_rtp' from '${sending}' to '${receiving}' with amount '29.00'
    should insert transaction to ipa '2' record to database
    close ipa database connection
    disconnect mq
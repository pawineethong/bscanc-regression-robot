*** Setting ***
Library     RequestsLibrary
Library     Collections
Library     DateTime
Library     String
Library     DatabaseLibrary
Library     ../pythonlibs/CSVLibrary.py
Library     ../pythonlibs/connect_mq.py
Library     ../pythonlibs/FileKeyword.py
Library     ../pythonlibs/ConnectionDB.py
Library     ../pythonlibs/StringKeyword.py
Variables   configs/dev/env_config.yaml
Variables   testdata/nft/test_data.yaml
Variables   testdata/common_response_code.yaml
Resource    ../keywords/api/common_db_keywords.robot
Resource    ../keywords/api/common_keywords.robot
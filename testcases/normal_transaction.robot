*** Settings ***
Resource  ../../resources/imports.robot
Resource  ../../keywords/api/bscanc_keyword.robot
Library     DatabaseLibrary

*** Test Cases ***
TC001_ISO20022_BSC_NORMAL Request and payment message (Accept)
    [Setup]    run keywords    mq manager connection site1
    ...                        connect to ipa database
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.99'
    And connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.99'
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.99'
    Then should insert transaction to ipa '6' record to database
    And should insert status 'ACCEPTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    disconnect mq

#TC001_ISO20022_BSC_NORMAL Request and payment message (Accept)
#    [Setup]    run keywords    mq manager connection site1
#    ...                        connect to ipa database
#    Given connect mq request to ips of bank 'ktb'
#    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.99'
#    And connect mq request ct to ips of bank 'cmbt'
#    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.99'
#    And connect mq respons to ips from bank 'ktb'
#    And sleep  45s
##    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.99'
#    And send response ct 'stop_camp' from 'ktb' to 'cmbt' with amount '29.99'
##    Then should insert transaction to ipa '6' record to database
##    And should insert status 'ACCEPTED' to ipa
#    [Teardown]  run keywords    close ipa database connection
#    ...    disconnect mq

#TC001_ISO20022_BSC_NORMAL Request and payment message (Accept)
#    [Setup]    run keywords    mq manager connection site1
#    ...                        connect to ipa database
#    Given connect mq request to ips of bank 'bbl'
#    When send singon request to bank 'bbl' with body 'signon'
#    Given connect mq request to ips of bank 'bay'
#    When send request rtp 'leg1_rtp' from 'bay' to 'bbl' with amount '29.99'
#    And connect mq request ct to ips of bank 'bbl'
#    And send request ct 'leg1_ct' from 'bbl' to 'bay' with amount '29.99'
#    And connect mq respons to ips from bank 'bay'
#    And send response ct 'leg3_ct' from 'bay' to 'bbl' with amount '29.99'
#    Then should insert transaction to ipa '6' record to database
#    And should insert status 'ACCEPTED' to ipa
#    [Teardown]  run keywords    close ipa database connection
#    ...    disconnect mq


TC002_ISO20022_BSC_NORMAL Request and response message (Reject)
    [Setup]    run keywords    mq manager connection site1
    ...                        connect to ipa database
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    And connect mq respons to ips from bank 'cmbt'
    And send response rtp reject code 'invalid_des_account' from 'cmbt' to 'ktb' with amount '29.00'
    Then should insert transaction to ipa '4' record to database
    And should insert status 'REJECTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...                         disconnect mq

TC003_ISO20022_BSC_NORMAL Request to void transaction (Accept)
    [Setup]    run keywords    mq manager connection site1
    ...                        connect to ipa database
    #  RTP AND CT - SUCCESS TRANSACTIONf
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    Then connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.00'
#    And sleep  10s
    Then should insert transaction to ipa '6' record to database
    And should insert status 'ACCEPTED' to ipa
    #   VOID TRANSACTION
    Given connect mq request void to ips of bank 'cmbt'
    When send request void 'leg1_void' from 'cmbt' to 'ktb' with amount '29.00'
    Then connect mq respons to ips from bank 'ktb'
    And send response void 'leg3_void' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction to ipa '4' record to database
    And should insert status 'ACCEPTED' to ipa
   [Teardown]  run keywords    close ipa database connection
    ...                         disconnect mq

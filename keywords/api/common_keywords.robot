*** Keywords ***
disconnect mq
    disConnection    ${connect_mqmr1}

send singon to '${bank}' site1
    connect mq request to ips of bank '${bank}'
    send singon request to bank '${bank}' with body 'signon'

send singon to '${bank}' site2
    connect mq request to ips of bank '${bank}' site2
    send request sign on message 'signon' of 'ktb2cmbt' to MQ '${queue_mq2}'

send echo request to bank '${trans_bank}' with body '${xml}'
    set test variable    ${xml}
    set test variable    ${bank_code}    ${bank_code_${trans_bank}}
    set test variable    ${sending_bank_code}    ${bank_code}
    create end to end id
    create biz message id
    create message id
    create InstrId
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    prepare xml body for echo
    put_mq    ${queue}    ${xml_body}

send singon request to bank '${trans_bank}' with body '${xml}'
    set test variable    ${xml}
    set test variable    ${trans_bank}
    set test variable    ${bank_code}    ${bank_code_${trans_bank}}
    set test variable    ${sending_bank_code}    ${bank_code}
    create end to end id
    create biz message id
    create message id
    create InstrId
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    prepare xml body for sing on transaction leg1
    put_mq    ${queue}    ${xml_body}

send singoff request to bank '${trans_bank}' with body '${xml}'
    set test variable    ${xml}
    set test variable    ${trans_bank}
    set test variable    ${bank_code}    ${bank_code_${trans_bank}}
    set test variable    ${sending_bank_code}   ${bank_code}
    create end to end id
    create biz message id
    create message id
    create InstrId
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    prepare xml body for sing off transaction leg1
    put_mq    ${queue}    ${xml_body}

prepare xml body for echo
     ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/echo/${xml}.xml
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    bank_code=${bank_code}
     ...    instr_id=${instr_id}
     set test variable     ${xml_body}

should insert status '${trans_status}' to ipa
     ${total_trans_ipa}=    query transaction of mt_transaction
     set test variable    ${total_trans_ipa}
     ${status}=   set variable    ${total_trans_ipa["TRANSACTION_STATUS_CODE"]}
     should be equal as strings    ${trans_status}     ${status}
should insert DUPL flag '${flag}' to ipa
     ${db_flag}=   set variable    ${total_trans_ipa["DUPLICATE"]}
     should be equal as strings    ${flag}     ${db_flag}

should insert transaction ct to database
    Wait Until Keyword Succeeds    2s    60s   should be insert rtp and ct to database are not empty '4'

should insert transaction to database
      Wait Until Keyword Succeeds    2s    60s    should be insert to database
      should be equal as strings    ${total_trans_ipb}    4

should insert transaction rtp and ct to database
     Wait Until Keyword Succeeds    1s    60s   should be insert rtp and ct to database are not empty '6'

should insert transaction rtp/ct and camt056 to database
     Wait Until Keyword Succeeds    1s    90s   should be insert rtp and ct to database are not empty '7'

should not be insert transaction database
    ${total_trans_ipa}=    query number of transaction by instr id
    should be equal as strings    ${total_trans_ipa}    ${0}

should be insert rtp and ct to database are not empty '${recode}'
    ${total_trans_ipa}=    query number of transaction by instr id
    should not be equal      ${total_trans_ipa}    ${0}
    should be equal as strings    ${total_trans_ipa}    ${recode}

should insert transaction to ipa '${number}' record to database
     wait Until Keyword Succeeds    1s    60s   should be insert rtp and ct to database are not empty '${number}'

should be insert to database
    ${total_trans_ipb}=    query number of transaction by transaction id
    should not be equal      ${total_trans_ipb}    ${0}
    set test variable    ${total_trans_ipb}

should insert transaction rtp to database
     Wait Until Keyword Succeeds    2s    50s   should be insert rtp and ct to database are not empty '4'
     should be equal as strings    ${total_trans_ipa}    4

mq manager connection site1
    ${connect_mqmr1}=    connectMQmanager    ${site1.queue_manager}    ${site1.channel}    ${site1.host}    ${site1.port}
     set test variable    ${connect_mqmr1}

mq manager connection site2
    ${connect_mqmr2}=    connectMQmanager    ${site2.queue_manager}    ${site2.channel}    ${site2.host}    ${site2.port}
     set test variable    ${connect_mqmr2}

connect mq request to ips of bank '${q_bank}'
    ${queue}=    connectQueue    ${connect_mqmr1}    ${mq.request.${q_bank}}
    set test variable    ${queue}

connect mq request to ips of bank '${q_bank}' site2
    ${queue_mq2}=    connectQueue    ${connect_mqmr2}    ${mq.request.site2.${q_bank}}
    set test variable    ${queue_mq2}

connect mq request ct to ips of bank '${q_bank}'
    ${queue}=    connectQueue    ${connect_mqmr1}    ${mq.request.${q_bank}}
    set test variable    ${connect_mqmr1}
    set test variable    ${queue}

connect mq request void to ips of bank '${q_bank}'
    connect mq request ct to ips of bank '${q_bank}'

connect mq respons to ips from bank '${q_bank}'
    ${queue}=    connectQueue    ${connect_mqmr1}    ${mq.response.${q_bank}}
    set test variable    ${queue}

send request message av '${xml}' of '${trans_bank}' to MQ
    set test variable    ${xml}
    set test variable    ${trans_bank}
    set test variable    ${bank_code}    ${${trans_bank}.sending_bank}
    create end to end id
    create biz message id
    create message id
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    prepare xml body for av transaction leg1
    put_mq    ${queue}    ${xml_body}

send request message ct '${xml}' of '${trans_bank}' to MQ
    set test variable    ${xml}
    set test variable    ${trans_bank}
    set test variable    ${bank_code}    ${${trans_bank}.sending_bank}
    create end to end id
    create biz message id
    create message id
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    prepare xml body for ct transaction leg1
    put_mq    ${queue}    ${xml_body}

send request sign off message '${xml}' to '${recve_bank}'
    set test variable    ${xml}
    set test variable    ${bank_code}    ${bank_code_${recve_bank}}
    set test variable    ${sending_bank_code}    ${bank_code}
    create end to end id
    create biz message id
    create message id
    create InstrId singoff
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    prepare xml body for sing off transaction leg1
    put_mq    ${queue}    ${xml_body}

send request sign on message '${xml}' of '${trans_bank}' to MQ '${queue_name}'
    set test variable    ${xml}
    set test variable    ${trans_bank}
    set test variable    ${bank_code}    ${bank_code_${trans_bank}}
    create biz message id
    create message id
    create InstrId
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    prepare xml body for sing on transaction leg1
    put_mq    ${queue_name}    ${xml_body}

prepare xml body for sing off transaction leg1
     ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/sign_off/${xml}.xml
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    instr_id=${instr_id_signoff}
     ...    bank_code=${bank_code}
     set test variable     ${xml_body}

prepare xml body for sing on transaction leg1
     ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/sign_on/${xml}.xml
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    instr_id=${instr_id}
     ...    bank_code=${bank_code}
     set test variable     ${xml_body}

prepare xml body for sing off transaction leg3
     ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/sing_off/${xml}.xml
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    instr_id=${instr_id}
     ...    recivng_bank=${recivng_bank}
     set test variable     ${xml_body}

prepare xml body for ct transaction leg1
    ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/rtp/${xml}.xml
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    sending_bank_code=${sending_bank_code}
     ...    recve_bank_code=${recve_bank_code}
     ...    from_account_name=${${sending_bank}.account_name}
     ...    from_account_number=${${sending_bank}.account_number}
     ...    org_msg=${original_64base}
     ...    sending_bank_name=${bank_name_${sending_bank}}
     ...    to_account_name=${${recve_bank}.account_name}
     ...    to_account_number=${${recve_bank}.account_number}
     ...    amount=${amount}
     ...    instr_id=${instr_id}
     ...    date_yyyy_mm_dd=${date_yyyy_mm_dd}
     ...    txid=${rtp_instr_id}
     set test variable     ${xml_body}

prepare xml body for void transaction leg1
    ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/void/${xml}.xml
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    sending_bank_code=${sending_bank_code}
     ...    recve_bank_code=${recve_bank_code}
     ...    from_account_name=${${sending_bank}.account_name}
     ...    from_account_number=${${sending_bank}.account_number}
     ...    org_msg=${original_64base}
     ...    sending_bank_name=${bank_name_${sending_bank}}
     ...    to_account_name=${${recve_bank}.account_name}
     ...    to_account_number=${${recve_bank}.account_number}
     ...    amount=${amount}
     ...    instr_id=${instr_id}
     ...    date_yyyy_mm_dd=${date_yyyy_mm_dd}
     ...    txid=${rtp_instr_id}
     set test variable     ${xml_body}

prepare xml body for ct transaction leg3
     ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/rtp/${xml}.xml
     ...    ct_leg1_tran_id=${ct_leg1_tran_id}
     ...    ct_leg1_msg_id=${ct_leg1_msg_id}
     ...    ct_leg1_date=${ct_leg1_date}
     ...    biz_msg_id=${biz_msg_id}
     ...    msg_id=${msg_id}
     ...    cur_date=${cur_date}
     ...    sending_bank_code=${sending_bank_code}
     ...    recve_bank_code=${recve_bank_code}
     ...    sending_bank_name=${bank_name_${sending_bank}}
     ...    instr_id=${ct_leg1_instr_id}
     ...    date_yyyy_mm_dd=${date_yyyy_mm_dd}
     ...    amt=${amount}
     ...    txid=${rtp_instr_id}
     ...    org_msg=${original_64base}
     ...    response_code=${response_code.invalid_des_account}
     set test variable     ${xml_body}

prepare xml body for void transaction leg3
     ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/void/${xml}.xml
     ...    void_leg1_tran_id=${void_leg1_tran_id}
     ...    void_leg1_msg_id=${void_leg1_msg_id}
     ...    void_leg1_date=${void_leg1_date}
     ...    biz_msg_id=${biz_msg_id}
     ...    msg_id=${msg_id}
     ...    cur_date=${cur_date}
     ...    sending_bank_code=${sending_bank_code}
     ...    recve_bank_code=${recve_bank_code}
     ...    sending_bank_name=${bank_name_${sending_bank}}
     ...    instr_id=${void_leg1_instr_id}
     ...    date_yyyy_mm_dd=${date_yyyy_mm_dd}
     ...    amt=${amount}
     ...    txid=${rtp_instr_id}
     ...    org_msg=${original_64base}
     set test variable     ${xml_body}

prepare xml body for ct/av transaction leg1
     ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/av/${xml}.xml
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    sending_bank=${${trans_bank}.sending_bank}
     ...    recv_bank=${${trans_bank}.recivng_bank}
     ...    from_account_name=${${trans_bank}.from_account_name}
     ...    from_account=${${trans_bank}.from_account}
     ...    to_display_name=${${trans_bank}.to_display_name}
     ...    to_account_name=${${trans_bank}.to_account_name}
     ...    to_account=${${trans_bank}.to_account}
     ...    recv_bank_name=${${trans_bank}.recv_bank_name}
     ...    org_msg=${${trans_bank}.org_msg}
     ...    amt=${${trans_bank}.amt}
     ...    instr_id=${instr_id}
     ...    date_yyyy_mm_dd=${date_yyyy_mm_dd}
     ...    txid=${rtp_instr_id}
     set test variable     ${xml_body}
prepare xml body for rtp transaction leg1
     ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/rtp/${xml}.xml
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    sending_bank_code=${sending_bank_code}
     ...    recv_bank_code=${recve_bank_code}
     ...    from_account_name=${${sending_bank}.account_name}
     ...    from_account_number=${${sending_bank}.account_number}
     ...    org_msg=${original_64base}
     ...    amt=${amount}
     ...    instr_id=${instr_id}
     ...    rtp_instr_id=${rtp_instr_id}
     ...    date_yyyy_mm_dd=${date_yyyy_mm_dd}
     set test variable     ${xml_body}

send request rtp '${xml}' from '${sending_bank}' to '${recve_bank}' with amount '${amount}'
    set test variable    ${xml}
    set test variable    ${sending_bank_code}    ${bank_code_${sending_bank}}
    set test variable    ${recve_bank_code}    ${bank_code_${recve_bank}}
    set test variable    ${sending_bank}
    set test variable    ${recve_bank}
    set test variable    ${amount}
    create end to end id
    create biz message id
    create message id
    create InstrId
    create rtp instrId
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    ${date_yyyy_mm_dd}=    convert current date time from db to Y-m-d
    set test variable    ${cur_date}
    set test variable    ${date_yyyy_mm_dd}
    original message rtp 'leg1_rtp' 64 base
    prepare xml body for rtp transaction leg1
    put_mq    ${queue}    ${xml_body}
    set test variable    ${rtp_leg1_msg_id}    ${msg_id}
    set test variable    ${rtp_leg1_date}    ${cur_date}
    set test variable    ${rtp_leg1_tran_id}    ${tran_id}
    set test variable    ${rtp_leg1_instr_id}    ${instr_id}

original message '${trans_type}' 64 base
    ${xml_org_msg} =    read_template_file    ${CURDIR}/../../resources/testdata/common/rtp/org_msg_${trans_type}.xml
     ...    merchant=${merchant_type.internet}
     ...    team_type=${team_type.mobile}
     ...    type_of_recv=${type_of_recev}
     ...    type_of_send=${type_of_send}
     ...    instr_id=${instr_id}
     ...    recv_taxid=${recv_taxid}
     ...    send_tax_id=${send_tax_id}
     ...    merchant_bill_id=${merchant_bill_id}
     ...    cus_dis_name=${cus_dis_name}
     ...    bill_dis_name_th=${bill_dis_name_th}
     ...    bill_dis_name_en=${bill_dis_name_en}
     ...    ref1=${ref1}
     ...    ref2=${ref2}
     ...    ref3=${ref3}
     ...    settle_date=${date_yyyy_mm_dd}
     ...    proxy_type=${${recve_bank}.proxy_type_bill}
     ...    proxy_value=${${recve_bank}.proxy_value_bill}
     ...    ctry_code=${country_code}
     ...    vat_rate=${vat_rate}
     ...    vat=${vat}
     ...    type_income=${type_income}
     ...    tax_rate=${tax_rate}
     ...    tax=${tax}
     ...    tax_condition=${tax_condition}
     ...    add_note=${add_note}
    ${original_64base}=    encode_base64    ${xml_org_msg}
    set test variable    ${original_64base}

original message ct leg3 '${trans_type}' 64 base
    ${xml_org_msg} =    read_template_file    ${CURDIR}/../../resources/testdata/common/rtp/org_msg_${trans_type}.xml
     ...    merchant=${merchant_type.internet}
     ...    team_type=${team_type.mobile}
     ...    type_of_recv=${type_of_recev}
     ...    type_of_send=${type_of_send}
     ...    instr_id=${instr_id}
     ...    recv_taxid=${recv_taxid}
     ...    send_tax_id=${send_tax_id}
     ...    merchant_bill_id=${merchant_bill_id}
     ...    cus_dis_name=${cus_dis_name}
     ...    bill_dis_name_th=${bill_dis_name_th}
     ...    bill_dis_name_en=${bill_dis_name_en}
     ...    ref1=${ref1}
     ...    ref2=${ref2}
     ...    ref3=${ref3}
     ...    settle_date=${date_yyyy_mm_dd}
     ...    proxy_type=${${recve_bank}.proxy_type_bill}
     ...    proxy_value=${${recve_bank}.proxy_value_bill}
     ...    ctry_code=${country_code}
     ...    vat_rate=${vat_rate}
     ...    vat=${vat}
     ...    type_income=${type_income}
     ...    tax_rate=${tax_rate}
     ...    tax=${tax}
     ...    tax_condition=${tax_condition}
     ...    add_note=${add_note}
     ...    ct_leg1_tran_id=${ct_leg1_tran_id}
     ...    ct_leg1_msg_id=${ct_leg1_msg_id}
     ...    ct_leg1_date=${ct_leg1_date}
     ...    biz_msg_id=${biz_msg_id}
     ...    msg_id=${msg_id}
     ...    cur_date=${cur_date}
     ...    sending_bank_code=${sending_bank_code}
     ...    recve_bank_code=${recve_bank_code}
     ...    sending_bank_name=${bank_name_${sending_bank}}
     ...    instr_id=${ct_leg1_instr_id}
     ...    date_yyyy_mm_dd=${date_yyyy_mm_dd}
     ...    amt=${amount}
     ...    txid=${rtp_instr_id}
     ...    response_code=${response_code.invalid_des_account}
    ${original_64base}=    encode_base64    ${xml_org_msg}
    set test variable    ${original_64base}

original message ct leg1 '${trans_type}' 64 base
    ${xml_org_msg} =    read_template_file    ${CURDIR}/../../resources/testdata/common/rtp/org_msg_${trans_type}.xml
     ...    merchant=${merchant_type.internet}
     ...    team_type=${team_type.mobile}
     ...    type_of_recv=${type_of_recev}
     ...    type_of_send=${type_of_send}
     ...    instr_id=${instr_id}
     ...    recv_taxid=${recv_taxid}
     ...    send_tax_id=${send_tax_id}
     ...    merchant_bill_id=${merchant_bill_id}
     ...    cus_dis_name=${cus_dis_name}
     ...    bill_dis_name_th=${bill_dis_name_th}
     ...    bill_dis_name_en=${bill_dis_name_en}
     ...    ref1=${ref1}
     ...    ref2=${ref2}
     ...    ref3=${ref3}
     ...    settle_date=${date_yyyy_mm_dd}
     ...    proxy_type=${${recve_bank}.proxy_type_bill}
     ...    proxy_value=${${recve_bank}.proxy_value_bill}
     ...    ctry_code=${country_code}
     ...    vat_rate=${vat_rate}
     ...    vat=${vat}
     ...    type_income=${type_income}
     ...    tax_rate=${tax_rate}
     ...    tax=${tax}
     ...    tax_condition=${tax_condition}
     ...    add_note=${add_note}
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    sending_bank_code=${sending_bank_code}
     ...    recve_bank_code=${recve_bank_code}
     ...    from_account_name=${${sending_bank}.account_name}
     ...    from_account_number=${${sending_bank}.account_number}
     ...    sending_bank_name=${bank_name_${sending_bank}}
     ...    to_account_name=${${recve_bank}.account_name}
     ...    to_account_number=${${recve_bank}.account_number}
     ...    amount=${amount}
     ...    instr_id=${instr_id}
     ...    date_yyyy_mm_dd=${date_yyyy_mm_dd}
     ...    txid=${rtp_instr_id}

    ${original_64base}=    encode_base64    ${xml_org_msg}
    set test variable    ${original_64base}

original message rtp '${trans_type}' 64 base
    ${xml_org_msg} =    read_template_file    ${CURDIR}/../../resources/testdata/common/rtp/org_msg_${trans_type}.xml
     ...    merchant=${merchant_type.internet}
     ...    team_type=${team_type.mobile}
     ...    type_of_recv=${type_of_recev}
     ...    type_of_send=${type_of_send}
     ...    instr_id=${instr_id}
     ...    recv_taxid=${recv_taxid}
     ...    send_tax_id=${send_tax_id}
     ...    merchant_bill_id=${merchant_bill_id}
     ...    cus_dis_name=${cus_dis_name}
     ...    bill_dis_name_th=${bill_dis_name_th}
     ...    bill_dis_name_en=${bill_dis_name_en}
     ...    ref1=${ref1}
     ...    ref2=${ref2}
     ...    ref3=${ref3}
     ...    settle_date=${date_yyyy_mm_dd}
     ...    proxy_type=${${recve_bank}.proxy_type_bill}
     ...    proxy_value=${${recve_bank}.proxy_value_bill}
     ...    ctry_code=${country_code}
     ...    vat_rate=${vat_rate}
     ...    vat=${vat}
     ...    type_income=${type_income}
     ...    tax_rate=${tax_rate}
     ...    tax=${tax}
     ...    tax_condition=${tax_condition}
     ...    add_note=${add_note}
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    sending_bank_code=${sending_bank_code}
     ...    recv_bank_code=${recve_bank_code}
     ...    from_account_name=${${sending_bank}.account_name}
     ...    from_account_number=${${sending_bank}.account_number}
     ...    amt=${amount}
     ...    instr_id=${instr_id}
     ...    rtp_instr_id=${rtp_instr_id}
    ${original_64base}=    encode_base64    ${xml_org_msg}
    set test variable    ${original_64base}

send request ct '${xml}' from '${sending_bank}' to '${recve_bank}' with amount '${amount}'
    set test variable    ${xml}
    set test variable    ${sending_bank_code}    ${bank_code_${sending_bank}}
    set test variable    ${recve_bank_code}    ${bank_code_${recve_bank}}
    set test variable    ${sending_bank}
    set test variable    ${recve_bank}
    set test variable    ${amount}
#    get datetime and genarate endtoend id
    create transaction id for ct
    create biz message id
    create message id
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    ${date_yyyy_mm_dd}=    convert current date time from db to Y-m-d
    set test variable    ${cur_date}
    set test variable    ${date_yyyy_mm_dd}
    original message ct leg1 'leg1_ct' 64 base
    prepare xml body for ct transaction leg1
    put_mq    ${queue}    ${xml_body}
    set test variable    ${ct_leg1_msg_id}    ${msg_id}
    set test variable    ${ct_leg1_date}    ${cur_date}
    set test variable    ${ct_leg1_tran_id}    ${tran_id}
    set test variable    ${ct_leg1_instr_id}    ${instr_id}

send request void '${xml}' from '${sending_bank}' to '${recve_bank}' with amount '${amount}'
    sleep  1s
    set test variable    ${xml}
    set test variable    ${sending_bank_code}    ${bank_code_${sending_bank}}
    set test variable    ${recve_bank_code}    ${bank_code_${recve_bank}}
    set test variable    ${sending_bank}
    set test variable    ${recve_bank}
    set test variable    ${amount}
#    get datetime and genarate endtoend id
    create transaction id for ct
    create biz message id
    create message id
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    ${date_yyyy_mm_dd}=    convert current date time from db to Y-m-d
    set test variable    ${cur_date}
    set test variable    ${date_yyyy_mm_dd}
    original message 'leg1_ct' 64 base
    create InstrId
    prepare xml body for void transaction leg1
    put_mq    ${queue}    ${xml_body}
    set test variable    ${void_leg1_msg_id}    ${msg_id}
    set test variable    ${void_leg1_date}    ${cur_date}
    set test variable    ${void_leg1_tran_id}    ${tran_id}
    set test variable    ${void_leg1_instr_id}    ${instr_id}

send response rtp reject code '${rc_number}' from '${sending_bank}' to '${recve_bank}' with amount '${amount}'
    sleep  2s
    set test variable    ${response_code}    ${response_code.${rc_number}}
    set test variable    ${sending_bank_code}    ${bank_code_${sending_bank}}
    set test variable    ${recve_bank_code}    ${bank_code_${recve_bank}}
    set test variable    ${sending_bank}
    set test variable    ${recve_bank}
    set test variable    ${amount}
    create transaction id for ct
    ${rrn}=    random_12_digits
    set test variable    ${rrn}
    create biz message id
    create message id
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    prepare xml body for rtp transaction leg3
    put_mq    ${queue}    ${xml_body}

send response reject void transaction '${rc_number}' from '${sending_bank}' to '${recve_bank}' with amount '${amount}'
    sleep  2s
    set test variable    ${response_code}    ${response_code.${rc_number}}
    set test variable    ${sending_bank_code}    ${bank_code_${sending_bank}}
    set test variable    ${recve_bank_code}    ${bank_code_${recve_bank}}
    set test variable    ${sending_bank}
    set test variable    ${recve_bank}
    set test variable    ${amount}
    create transaction id for ct
    ${rrn}=    random_12_digits
    set test variable    ${rrn}
    create biz message id
    create message id
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    prepare xml body for void transaction leg3 reject
    put_mq    ${queue}    ${xml_body}

send response message av '${xml}' to MQ
    set test variable    ${xml}
    prepare xml body for av transaction leg3
    put_mq    ${queue}    ${xml_body}

send response ct '${xml}' from '${sending_bank}' to '${recve_bank}' with amount '${amount}'
    sleep  2s
    set test variable    ${xml}
    set test variable    ${sending_bank_code}    ${bank_code_${sending_bank}}
    set test variable    ${recve_bank_code}    ${bank_code_${recve_bank}}
    set test variable    ${sending_bank}
    set test variable    ${recve_bank}
    set test variable    ${amount}
    create transaction id for ct
    ${rrn}=    random_12_digits
    set test variable    ${rrn}
    create biz message id
    create message id
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    original message ct leg3 'leg3_ct' 64 base
    prepare xml body for ct transaction leg3
    put_mq    ${queue}    ${xml_body}

send response void '${xml}' from '${sending_bank}' to '${recve_bank}' with amount '${amount}'
    sleep  2s
    set test variable    ${xml}
    set test variable    ${sending_bank_code}    ${bank_code_${sending_bank}}
    set test variable    ${recve_bank_code}    ${bank_code_${recve_bank}}
    set test variable    ${sending_bank}
    set test variable    ${recve_bank}
    set test variable    ${amount}
    create transaction id for ct
    ${rrn}=    random_12_digits
    set test variable    ${rrn}
    create biz message id
    create message id
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    original message 'leg3_ct' 64 base
    prepare xml body for void transaction leg3
    put_mq    ${queue}    ${xml_body}


#send response ct/av message '${xml}' to MQ
#    set test variable    ${xml}
##    create transaction id for ct
#    ${rrn}=    random_12_digits
#    set test variable    ${rrn}
#    create biz message id
#    create message id
#    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
#    set test variable    ${cur_date}
#    prepare xml body for ct/av transaction leg3
#    put_mq    ${queue}    ${xml_body}

send response sign off message '${xml}' to MQ
    set test variable    ${xml}
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    prepare xml body for sing off transaction leg3
    put_mq    ${queue}    ${xml_body}

send response message rtp '${xml}' to MQ
    sleep  1s
    set test variable    ${xml}
    set test variable    ${response_code}    ${${trans_bank}.response_code.insufficient_funds}
    create transaction id for ct
    ${rrn}=    random_12_digits
    set test variable    ${rrn}
    create biz message id
    create message id
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    set test variable    ${cur_date}
    prepare xml body for rtp transaction leg3
    put_mq    ${queue}    ${xml_body}

prepare xml body for rtp transaction leg3
     ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/rtp/leg3_rtp_reject.xml
     ...    rtp_leg1_tran_id=${rtp_leg1_tran_id}
     ...    rtp_leg1_msg_id=${rtp_leg1_msg_id}
     ...    rtp_leg1_date=${rtp_leg1_date}
     ...    biz_msg_id=${biz_msg_id}
     ...    msg_id=${msg_id}
     ...    current_date=${cur_date}
     ...    sending_bank_code=${sending_bank_code}
     ...    recve_bank_code=${recve_bank_code}
     ...    sending_bank_name=${bank_name_${sending_bank}}
     ...    instr_id=${rtp_leg1_instr_id}
     ...    date_yyyy_mm_dd=${date_yyyy_mm_dd}
     ...    amt=${amount}
     ...    response_code=${response_code}
     ...    org_msg=${original_64base}
     ...    rtp_instr_id=${rtp_instr_id}
     set test variable     ${xml_body}

prepare xml body for void transaction leg3 reject
     ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/void/leg3_void_reject.xml
     ...    void_leg1_tran_id=${void_leg1_tran_id}
     ...    void_leg1_msg_id=${void_leg1_msg_id}
     ...    void_leg1_date=${void_leg1_date}
     ...    biz_msg_id=${biz_msg_id}
     ...    msg_id=${msg_id}
     ...    cur_date=${cur_date}
     ...    sending_bank_code=${sending_bank_code}
     ...    recve_bank_code=${recve_bank_code}
     ...    sending_bank_name=${bank_name_${sending_bank}}
     ...    instr_id=${void_leg1_instr_id}
     ...    date_yyyy_mm_dd=${date_yyyy_mm_dd}
     ...    amt=${amount}
     ...    txid=${rtp_instr_id}
     ...    org_msg=${original_64base}
     ...    response_code=${response_code}
     set test variable     ${xml_body}

prepare xml body for ct/av transaction leg3
     ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/av/${xml}.xml
     ...    ct_leg1_tran_id=${ct_leg1_tran_id}
     ...    ct_leg1_msg_id=${ct_leg1_msg_id}
     ...    ct_leg1_date=${ct_leg1_date}
     ...    biz_msg_id=${biz_msg_id}
     ...    msg_id=${msg_id}
     ...    cur_date=${cur_date}
     ...    sending_bank=${${trans_bank}.sending_bank}
     ...    recv_bank=${${trans_bank}.recivng_bank}
     ...    sending_bank_name=${${trans_bank}.sending_bank_name}
     ...    recv_bank_name=${${trans_bank}.recv_bank_name}
     ...    instr_id=${ct_leg1_instr_id}
     ...    date_yyyy_mm_dd=${date_yyyy_mm_dd}
     ...    amt=${${trans_bank}.amt}
     ...    txid=${txid}
     set test variable     ${xml_body}

prepare xml body for av transaction leg3
    ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/av/${xml}.xml
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    sending_bank=${${trans_bank}.sending_bank}
     ...    recv_bank=${${trans_bank}.recivng_bank}
     ...    proxy_type=${${trans_bank}.proxy.type}
     ...    proxy_value=${${trans_bank}.proxy.value}
     ...    org_msg=${${trans_bank}.org_msg}
     ...    to_display_name=${${trans_bank}.to_display_name}
     ...    to_account_name=${${trans_bank}.to_account_name}
     ...    to_account=${${trans_bank}.to_account}
     set test variable     ${xml_body}

prepare xml body for av transaction leg1
    ${xml_body} =    read_template_file    ${CURDIR}/../../resources/testdata/common/av/${xml}.xml
     ...    transaction_id=${tran_id}
     ...    biz_msg_id=${biz_msg_id}
     ...    current_date=${cur_date}
     ...    msg_id=${msg_id}
     ...    sending_bank=${${trans_bank}.sending_bank}
     ...    proxy_type=${${trans_bank}.proxy.type}
     ...    proxy_value=${${trans_bank}.proxy.value}
     ...    org_msg=${${trans_bank}.org_msg}
     set test variable     ${xml_body}

create end to end id
    ${date}=    Get Current Date
    ${datetime}=    Convert Date    ${date}    datetime
    ${trace}=    random_6_digits
    ${rrn}=    random_12_digits
    ${tran_id}=    set variable    ${datetime.month}${datetime.day}${datetime.hour}${datetime.minute}${datetime.second}${trace}${rrn}
    ${tran_id}=    convert to string    ${tran_id}
    set test variable    ${tran_id}
    set test variable    ${datetime}
    set test variable    ${trace}
    set test variable    ${rrn}

create transaction id
    ${date_format}=    Convert Date    ${datetime}    result_format=%Y%m%d
    ${txid}=    set variable    ${date_format}${recve_bank_code}B${rrn}
    ${txid}=    convert to string    ${txid}
    set test variable    ${txid}

create transaction identification
    ${date_format}=    Convert Date    ${datetime}    result_format=%Y%m%d
#    ${txid}=    set variable    R${recve_bank_code}9${rrn}
    ${txid}=    set variable    R${recve_bank_code}9071${trace}22
    ${txid}=    convert to string    ${txid}
    set test variable    ${txid}

create transaction id for ct
    ${date}=    Get Current Date
    ${datetime}=    Convert Date    ${date}    datetime
    ${trace}=    random_6_digits
    ${tran_id}=    set variable    ${datetime.month}${datetime.day}${datetime.hour}${datetime.minute}${datetime.second}${trace}${rrn}
    ${tran_id}=    convert to string    ${tran_id}
    set test variable    ${tran_id}
    set test variable    ${datetime}
    log to console    ${tran_id}

create biz message id
    ${biz_msg_id}=    set variable    B1${datetime.year}${datetime.month}${datetime.day}${datetime.hour}${datetime.minute}${datetime.second}${rrn}
    ${biz_msg_id}=    convert to string    ${biz_msg_id}
    set test variable    ${biz_msg_id}

create message id
    ${msg_id}=    set variable    M1${datetime.year}${datetime.month}${datetime.day}${datetime.hour}${datetime.minute}${datetime.second}${sending_bank_code}${rrn}
    ${msg_id}=    convert to string    ${msg_id}
    set test variable    ${msg_id}

create InstrId
    ${date_format}=    Convert Date    ${datetime}    result_format=%Y%m%d%H%M%S
    ${instr_id}=    set variable    ${date_format}${trace}${sending_bank_code}${rrn}
    ${instr_id}=    convert to string    ${instr_id}
    set test variable    ${instr_id}

create InstrId singoff
    ${date_format}=    Convert Date    ${datetime}    result_format=%Y%m%d%H%M%S
    ${instr_id}=    set variable    ${date_format}${trace}${sending_bank_code}${rrn}
    ${instr_id_signoff}=    convert to string    ${instr_id}
    set test variable    ${instr_id_signoff}

create rtp instrId
    ${date}=    Get Current Date
    ${datetime}=    Convert Date    ${date}    datetime
    ${yyyy-mm-dd}=    Convert Date    ${datetime}    result_format=%Y.%m.%d
    ${julian_date} =   create_julian_date    ${yyyy-mm-dd}
    run keyword if    ${julian_date} < ${100}    add zero to julian_date '${julian_date}'
    log to console    ${julian_date}
    ${rtp_instr_id}=    set variable    R${sending_bank_code}9${julian_date}${trace}99
    ${rtp_instr_id}=    convert to string    ${rtp_instr_id}
    set test variable    ${rtp_instr_id}

add zero to julian_date '${date}'
    set test variable    ${julian_date}    0${date}

create rpt id
    ${rtp_id}=    set variable    R${tran_id}
    ${rtp_id}=    convert to string    ${rtp_id}
    set test variable    ${rtp_id}

create data time
    ${date}=    Get Current Date
    ${datetime}=    Convert Date    ${date}    datetime
    ${senddate}=    set variable    ${datetime.year}${datetime.month}${datetime.day}
    ${sendtime}=    set variable    ${datetime.hour}${datetime.minute}${datetime.second}
    set test variable    ${senddate}
    set test variable    ${sendtime}

send requet to void transaction '${xml}' from '${sending_bank}' to '${recving_bank}'
    set test variable    ${xml}
    set test variable    ${trans_bank}
#    get datetime and genarate endtoend id
    create transaction id for ct
#    create bizMsg id
    create biz message id
#    create msgid
    create message id
#   create txid
    create transaction id
    ${cur_date}=   convert current date time from db to Y-m-dTH:M:S
    ${date_yyyy_mm_dd}=    convert current date time from db to Y-m-d
    set test variable    ${cur_date}
    set test variable    ${date_yyyy_mm_dd}
    prepare xml body for ct transaction leg1
    put_mq    ${queue}    ${xml_body}
    set test variable    ${ct_leg1_msg_id}    ${msg_id}
    set test variable    ${ct_leg1_date}    ${cur_date}
    set test variable    ${ct_leg1_tran_id}    ${tran_id}
    set test variable    ${ct_leg1_instr_id}    ${instr_id}

#get day of current date
#    ${date} =	Get Current Date
#    ${datetime} =	Convert Date    ${date}    datetime
#    [Return]      ${datetime.day}
#
#convert date time from db
#    [Arguments]  ${date_from_db}
#    ${date_convert}=    Convert Date   ${date_from_db}
#    ${date_format}=    Convert Date    ${date_convert}    result_format=%d/%m/%Y %H:%M:%S
#    [Return]    ${date_format}
#
#convert date time from db to d/m/Y
#    [Arguments]  ${date_from_db}
#    ${date_convert}=    Convert Date   ${date_from_db}
#    ${date_format}=    Convert Date    ${date_convert}    result_format=%d/%m/%Y
#    [Return]    ${date_format}
#
#convert date time from db to d/m/Y HMS
#    [Arguments]  ${date_from_db}
#    ${date_convert}=    Convert Date   ${date_from_db}
#    ${date_format}=    Convert Date    ${date_convert}    result_format=%d/%m/%Y %H:%M:%S
#    [Return]    ${date_format}
#
#convert date time from db to Y-m-d
#    ${date_format}=    Convert Date    ${datetime}    result_format=%Y-%m-%d %H:%M:%S
#    [Return]    ${date_format}
#
#convert current date time from db to Y-m-d H:M:S:F
#    ${date} =	Get Current Date
#    ${date_format}=    Convert Date    ${date}    result_format=%Y-%m-%d %H:%M:%S.%f
#    [Return]    ${date_format}
#
convert current date time from db to Y-m-d
    ${date} =	Get Current Date
    ${date_format}=    Convert Date    ${date}    result_format=%Y-%m-%d
    [Return]    ${date_format}

convert current date time from db to Y-m-dTH:M:S
    ${date} =	Get Current Date
    ${date_format}=    Convert Date    ${date}    result_format=%Y-%m-%dT%H:%M:%S
    [Return]    ${date_format}

#convert current time from db to H:M:S:F
#    ${date} =	Get Current Date
#    ${date_format}=    Convert Date    ${date}    result_format=%H:%M:%S.%f
#    [Return]    ${date_format}
#
#get file from csv
#    Wait Until Created    ${CURDIR}/csv/*    10s
#    ${count_file}=    Count Files In Directory    ${CURDIR}/csv
#    ${list}=     List Files In Directory    ${CURDIR}/csv
#    ${file}    Get File    ${CURDIR}/csv/${list[0]}
#    ${list_length}=    get length    ${list}
#    set test variable    ${count_file}
#    set test variable    ${list}
#    set test variable    ${file}
#
#should be donwload to csv file
#    get file from csv
#    should not be equal    ${count_file}    ${0}
#
#Decode string to utf-8
#    [Arguments]    ${data}
#    ${string}=   Decode Bytes To String    ${data}    UTF-8
#    [Return]    ${string}

*** Settings ***
Resource  ../../resources/imports.robot
Resource  ../../keywords/api/bscanc_keyword.robot
Library     DatabaseLibrary

*** Test Cases ***
TC001_ISO20022_BSC_NW Member bank echo request to ips - Success
    [Setup]    mq manager connection site1
    Given connect mq request to ips of bank 'ktb'
    When send echo request to bank 'ktb' with body 'echo'
    And connect mq request to ips of bank 'cmbt'
    And send echo request to bank 'cmbt' with body 'echo'
    Then send request to rtp and ct success from 'ktb' to 'cmbt'

#TC002_ISO20022_BSC_NW Member bank singon request to ips - Success
#    [Setup]    mq manager connection site1
#    Given connect mq request to ips of bank 'cmbt'
#    When send singon request to bank 'cmbt' with body 'signon'
#    Then send request to rtp and ct success from 'ktb' to 'cmbt'

TC002_ISO20022_BSC_NW Member bank singon request to ips - Success
    [Setup]    mq manager connection site1
    Given connect mq request to ips of bank 'bbl'
    When send singon request to bank 'bbl' with body 'signon'
#    Then send request to rtp and ct success from 'ktb' to 'cmbt'

TC004_ISO20022_BSC_NW Member bank singoff request to ips - Success
    [Setup]    run keywords  mq manager connection site1
    ...                      send singon to 'ktb' site1
    ...                      connect to ipa database
    #  RTP AND CT - SUCCESS TRANSACTIONf
    Given connect mq request to ips of bank 'ktb'
    When send request rtp 'leg1_rtp' from 'ktb' to 'cmbt' with amount '29.00'
    Then connect mq request ct to ips of bank 'cmbt'
    And send request ct 'leg1_ct' from 'cmbt' to 'ktb' with amount '29.00'
    And connect mq respons to ips from bank 'ktb'
    And send response ct 'leg3_ct' from 'ktb' to 'cmbt' with amount '29.00'
    Then should insert transaction to ipa '6' record to database
    And should insert status 'ACCEPTED' to ipa
    # SIGN OFF KTB
    And connect mq request to ips of bank 'ktb'
    And sleep  2s
    And send request sign off message 'signoff' to 'ktb'
    And sleep  2s
    #   VOID TRANSACTION
    Given connect mq request void to ips of bank 'cmbt'
    When send request void 'leg1_void' from 'cmbt' to 'ktb' with amount '29.00'
    Then should insert transaction to ipa '2' record to database
    And should insert status 'REJECTED' to ipa
    [Teardown]  run keywords    close ipa database connection
    ...    send singon to 'ktb' site1
    ...    disconnect mq
